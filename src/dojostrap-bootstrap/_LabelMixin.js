define([
  'dojo/_base/declare',
  'jquery'
], function(declare, $) {
  return declare(null, {

    domNode: null,
    labelNode: null,

    _getLabelNodeAttr: function() {
      return this.domNode;
    },

    _getLabelAttr: function() {
      return $(this.get('labelNode')).html();
    },

    _setLabelAttr: function(value) {
      $(this.get('labelNode')).html(value);
    }

  });
});
