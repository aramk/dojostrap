define([
  'dojo/_base/declare',
  'jquery',
  './Base',
  './_LabelMixin',
  'dojostrap-core/Dropdown',
  'dijit/_TemplatedMixin',
  // Required
  'bootstrap/js/dropdown'
], function(declare, $, Base, _LabelMixin, Dropdown, _TemplatedMixin) {
  return declare([Base, _LabelMixin, Dropdown, _TemplatedMixin], {

    _type: 'dropdown',
    _methods: ['toggle'],
    templateString: '<div class="dropdown">\
      <a role="button" data-toggle="dropdown" data-target="#" href="#"><span class="caret"></span></a>\
      <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel"></ul></div>',

    itemTemplateString: '<li role="presentation"><a role="menuitem" tabindex="-1" href="${href}">${content}</a></li>',

    _getElement: function() {
      return this.get('dropdownButton');
    },

    _getLabelNodeAttr: function() {
      return $('a', this.domNode)[0];
    },

    _getDropdownButtonAttr: function() {
      // TODO(aramk) should we return strictly DOM or jQuery element?
      return $('[role=button]', this.domNode);
    },

    _getDropdownMenuAttr: function() {
      // TODO(aramk) should we return strictly DOM or jQuery element?
      return $('.dropdown-menu', this.domNode);
    },

    createItem: function(args) {
      return $(this._renderTemplate(this.itemTemplateString, args))[0];
    },

    addItem: function(args) {
      this.get('dropdownMenu').append(this.createItem(args));
    },

    addSeparator: function() {

    }

  });
})
;
