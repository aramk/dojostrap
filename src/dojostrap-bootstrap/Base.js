define([
  'dojo/_base/declare',
  'dojostrap-core/Base'
], function(declare, Base) {
  return declare([Base], {

    _type: null,
    _methods: [],

    postCreate: function() {
      this.inherited(arguments);
      this.initHandler();
      this._callTypeFunction();
    },

    initHandler: function() {
      this._methods.forEach(function(method) {
        this[method] = function() {
          var args = Array.prototype.slice.apply(arguments);
          return this._handler(method, args);
        }
      }, this);
    },

    _getElement: function() {
      return $(this.domNode);
    },

    _getTypeFunction: function() {
      return this._getElement()[this._type];
    },

    _callTypeFunction: function() {
      return this._getTypeFunction().apply(this._getElement());
    },

    _handler: function(method, args) {
      args = args || [];
      args = [method].concat(args);
      var elem = this._getElement();
      return elem[this._type].apply(elem, args);
    }

  });
});
