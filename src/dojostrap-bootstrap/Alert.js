define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'jquery',
  './Base',
  './_ModeMixin',
  'dojostrap-core/Alert',
  // Required
  'bootstrap/js/button'
], function(declare, lang, $, Base, _ModeMixin, Alert) {
  return declare([Base, Alert, _ModeMixin], {

    _type: 'alert',
    _methods: ['close'],
    templateString: '<div class="alert alert-warning">\
        <button type="button" class="close hidden" data-dismiss="alert" aria-hidden="true">&times;</button>\
        <div class="alert-content"></div>\
        </div>',
    isDismissable: false,

    _setIsDismissableAttr: function (isDismissable) {
      this.isDismissable = isDismissable;
      // TODO(aramk) abstact this
      $(this.domNode).toggleClass('alert-dismissable', isDismissable);
      $(this.get('closeNode')).toggleClass('hidden', !isDismissable);
    },

    _getIsDismissableAttr: function () {
      return this.isDismissable;
    },

    _getCloseNodeAttr: function () {
      return $('button', this.domNode);
    },

    _getValueNodeAttr: function () {
      return $('.alert-content', this.domNode);
    },

    // TODO(aramk) abstract this into Base.
    _setValueAttr: function (value) {
      $(this.get('valueNode')).html(value);
    },

    _getValueAttr: function () {
      return $(this.get('valueNode')).html();
    }

  });
});
