define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'jquery'
], function(declare, lang, $) {
  return declare(null, {

    // _type: String
    //    This is expected to exist as the string defining the type of widget.
    _type: null,
    _modes: ['danger', 'warning', 'success', 'info'],

    _setModeAttr: function (mode) {
      $.each(this._modes, lang.hitch(this, function (i, mode) {
        $(this.domNode).addClass(this._type + '-' + mode);
      }));
      $(this.domNode).addClass(this._type + '-' + mode);
    },

    _getModeAttr: function () {
      var _mode = null;
      $.each(this._modes, lang.hitch(this, function (i, mode) {
        if ($(this.domNode).hasClass(this._type + '-' + mode)) {
          _mode = mode;
          return false;
        }
      }));
      return _mode;
    }

  });
});
