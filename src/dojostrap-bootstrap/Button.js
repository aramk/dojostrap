define([
  'dojo/_base/declare',
  'jquery',
  './Base',
  './_LabelMixin',
  'dojostrap-core/Button',
  // Required
  'bootstrap/js/button'
], function(declare, $, Base, _LabelMixin, Button) {
  return declare([Base, _LabelMixin, Button], {

    _type: 'button',
    _methods: ['toggle'],
    templateString: '<button class="btn btn-primary" type="button"></button>'

  });
});
