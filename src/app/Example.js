/**
 * This file is a very simple example of a class declaration in Dojo. It defines the `app/Dialog` module as a new
 * class that extends a dijit Dialog and overrides the default title and content properties.
 */
define([
  'dojo/_base/declare',
  'dojo/dom-construct',
  // TODO(aramk) rename the library, to provocative.
  'dojostrap/Button',
  'dojostrap/Dropdown'
], function(declare, domConstruct, Button, Dropdown) {
  return declare(null, {

    constructor: function() {
      var button = new Button();
      button.set('label', 'Some Button');
      button.toggle();
      domConstruct.place(button.domNode, document.body);

      var dropdown = new Dropdown();
      domConstruct.place(dropdown.domNode, document.body);
      dropdown.set('label', 'Dropdown!');
      dropdown.addItem({
        href: 'test.com',
        content: 'Test'
      });

      button.on('click', function() {
        setTimeout(function() {
          dropdown.toggle();
        }, 10);
      });
    }

  });
});