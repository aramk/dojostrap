define([
  'dojo/_base/declare',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'dijit/_WidgetsInTemplateMixin',
  'dijit/_TemplatedMixin',
  'dojo/Evented',
  'dojo/string'
], function(declare, _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, _TemplatedMixin, Evented, string) {
  return declare([Evented, _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {

    _renderTemplate: function(template, data) {
      // summary:
      //		Does substitution of ${foo} type properties in template string
      // description:
      //    Taken from dijit/_TemplatedMixin.
      // tags:
      //		private
      var _this = this;
      // Cache contains a string because we need to do property replacement
      // do the property replacement
      return string.substitute(template, data, function(value, key) {
        if (key.charAt(0) == '!') {
          value = lang.getObject(key.substr(1), false, _this);
        }
        if (value == null) {
          return "";
        }
        // Substitution keys beginning with ! will skip the transform step,
        // in case a user wishes to insert unescaped markup, e.g. ${!foo}
        return key.charAt(0) == "!" ? value :
          // Safer substitution, see heading "Attribute values" in
          // http://www.w3.org/TR/REC-html40/appendix/notes.html#h-B.3.2
            value.toString().replace(/"/g, "&quot;"); //TODO: add &amp? use encodeXML method?
      }, this);
    }

  });
});
