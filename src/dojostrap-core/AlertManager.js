define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/topic',
  'dojostrap/Alert',
  'jquery'
], function(declare, lang, topic, Alert, $) {
  return declare(null, {

    _container: null,

    constructor: function() {
      this._container = $('<div class="alert-wrapper"><div class="alert-wrapper-content"></div></div>');
      topic.subscribe('dojostrap/alert', lang.hitch(this, this.alert));
      $(document.body).prepend(this._container);
      $(window).resize(lang.hitch(this, this._resize));
    },

    alert: function(args) {
      var alert = new Alert(args);
      alert.set('value', args.value);
      alert.set('isDismissable', true);
      if (args.callback) {
        args.callback(alert);
      }
      var $wrapper = $('<div></div>');
      $wrapper.append(alert.domNode);
      $(this.getContentNode()).append($wrapper);
      this._resize();
    },

    getContentNode: function() {
      return $('.alert-wrapper-content', this._container);
    },

    _resize: function() {
      var width = $(this._container).width();
      $(this._container).css('left', '50%');
      $(this._container).css('margin-left', -width/2 + 'px');
    }

  });
});
